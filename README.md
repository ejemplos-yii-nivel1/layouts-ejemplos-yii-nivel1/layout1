<p align="center">
    <h1 align="center">Layout base 1 Yii2 - Ejemplos Nivel 1</h1>
    <br>
</p>

<h2>Requisitos</h2>
Los requisitos minimos es que tengamos como minimo PHP 5.4

DESCRIPCION
-----------
Este es el primer layout que necesitaremos para los ejemplos de nivel 1.

INSTALACION
------------

### INSTALANDO MEDIANTE COMPOSER

En un terminal debeis escribir lo siguiente.

~~~
composer create-project ejemplos-yii-nivel1/layout1.git
~~~